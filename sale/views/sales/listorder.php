<?php
  require_once(__DIR__.'/../../config/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>List Orders</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js"></script>
  <style type="text/css">
    .btn-warning{
        float: right;
        margin-top: 30px;
        margin-bottom: 30px;
    }
    
    
    
  </style>
  <script>
 

  
      $(document).ready(function(){
          $('.button-get').click(function(e){
             e.preventDefault();
             orderID = $(this).data("order");
             $.ajax({
                url: './../../api/orderitem.php',
                dataType: 'json',
                data: "orderid="+orderID,
                success: function(response){
                  if(response.result==true){
                      console.log(response);

                    

                      template = $("#template-x").html();
                       var render = Mustache.render(template, response);
                      $("#orderitems").html(render);
                      console.log(render);
                      //var render = Mustache.render(template, {"loop": rows});
                      //console.log(render);
                    //$('#orderitem').html(render);
                    
                  }
                  
                }

             });
          });
      });
    
  </script>

</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
        <h2> Order Table </h2>


    <table class="table table-responsive table-striped table-bordered table-hover" id="table-orders">                     
    
        <thead>
            <tr>
              <th>ID</th>
              <th>MemberID</th>
              <th>TotalPrice</th>
              <th>Creation Date</th>
              <th>Items Info</th>

              
            </tr>
        </thead>
        <tbody>
        <?php
          $order = new Order();
          $orders=$order->getAll();
          if(count($orders)>0) {
            foreach($orders as $o) {
              print '<tr>
                <td>'.$o["id"].'</td>
                <td>'.$o["memberID"].'</td>
                <td>'.$o["totalPrice"].'</td>
                <td>'.$o["creationDate"].'</td>
                <td><a href="#" class="btn btn-info button-get" data-order="'.$o["id"].'">Get</a></td>
                </tr>';
            }
          }
        ?>
    
        </tbody>
      </table>
    </div>
    <div id="orderitems" class="col-sm-6">
    </div>
  </div>
</div>

<script id="template-x" type="text/template">
  <h2>Order Item Table</h2>

  <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
              <th>ID</th>
              <th>ProductID</th>
              <th>OrderID</th>
              <th>Amount</th>
              <th>Subtotal Price</th>
              <th>Creation Date</th>
            </tr>
        </thead>
        <tbody>
          {{#data}}
            <tr>
                <td>{{id}}</td>
                <td>{{productID}}</td>
                <td>{{orderID}}</td>
                <td>{{amount}}</td>
                <td>{{subtotalPrice}}</td>
                <td>{{creationDate}}</td>
            </tr>
        {{/data}}
        </tbody>
  </table>
</script>




</body>
</html>
 