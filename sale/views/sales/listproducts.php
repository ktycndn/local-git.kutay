<?php
  require_once(__DIR__.'/../../config/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
 <title>List Products</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js"></script>
  <style type="text/css">
    .btn-warning{
        float: right;
        margin-top: 30px;
        margin-bottom: 30px;
    }
    #second{
      display : none;
    }
    #third{
      display: none;
      margin-top: 56px;

    }
    #done{
      display : none;
    }
    
  </style>
    <script>
 

  
      $(document).ready(function(){
          $('#opt').change(function(e){
             e.preventDefault();
             name = $('#opt').val();
             console.log(name);
             $.ajax({
                url: './../../api/products.php',
                dataType: 'json',
                data: "name="+name,
                success: function(response){
                  if(response.result==true){
                      console.log(response);

                    $("#second").show();
                    $("#third").show();
                     template = $("#template-product").html();
                       var render = Mustache.render(template, response);
                      $("#producttable").html(render);


                    
                  }
                  
                }

             });

          });
          $('.button-get').click(function(e){
             e.preventDefault();
             name = $('#opt').val();
             amount = $('#amount').val();
             $.ajax({
                url: 'addcart.php',
                dataType: 'json',
                data: "name="+name+"&amount="+amount,
                success: function(response){
                  if(response.result==true){
                      console.log(response);

                    $("#done").show();


                    
                  }
                  
                }

             });
             
          });
      });
    
  </script>
</head>
<body>
    
  
  <div class="container-fluid">
    <div class="row">
      <h2> Product Table </h2>
    </div>
    <div class="row">
      <div class="col-sm-4">
        
        <h3> Select product to add cart </h3>
        <select class="selectpicker form-control" id="opt" name="opt" >
        <option disabled selected value> -- select a product -- </option>
  
  


<?php
        $product = new Product();
        $datas=$product->getAll();
        for($i=0;$i<sizeof($datas);$i++){
    
          print '<option value="'.$datas[$i]["name"].'">'.$datas[$i]["name"].'</option>';
          
        } 
?>
        
        </select>
        
           
      </div>



       <div class="col-sm-4" id="second" >
        
        <h3> Select amount to add cart </h3>
        <select class="selectpicker form-control" id="amount" name="amount" >
        <option disabled selected value> -- select an amount -- </option>
  
  


<?php
        $product = new Product();
        $datas=$product->getAll();
        for($i=1;$i<31;$i++){
    
          echo "<option value=\"".$i."\">".$i."</option>";
          
        } 
?>
        
        </select>
        
           
      </div>
      <div class="col-sm-2" id="third">
        <a href="#" class="btn btn-success button-get">Add</a>
      </div>

    </div>

      <div class="row">
      <div class="col-sm-10" id="producttable">
      </div>
      </div>
      <div class="row" id="done">
      <h2>This product is successfully added to cart</h2>
      </div>
      







    










  </div>

                
  <div class="container-fluid">

    <a class="btn btn-lg btn-warning" href="index.php" role="button">Go back</a>
  </div>

<script id="template-product" type="text/template">
  <h2>Product Table Table</h2>

  <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Creation Date</th>
              <th>Status</th>
            </tr>
        </thead>
        <tbody>
          {{#data}}
            <tr>
                <td>{{id}}</td>
                <td>{{name}}</td>
                <td>{{price}}</td>
                <td>{{creationDate}}</td>
                <td>{{status}}</td>
            </tr>
        {{/data}}
        </tbody>
  </table>
</script>



</body>
</html>
 