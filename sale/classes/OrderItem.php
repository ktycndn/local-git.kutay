<?php
/**
 * Class: OrderItem
 * @author Kutay Candan 
 * This is OrderItem object sale product.
 */ 
class OrderItem extends Thing {

	static $statuses = array(
		0	=> "draft",
		1	=> "active",
		2	=> "passive",
		3 	=> "deleted"
	);

	public function __construct() {
		$this->name = "orderItem";
		$this->table = "OrderItem";
		$this->prefix = "OrderItem";
		$this->fields = array(
			"id" => array(
				"name" 		=> "id",
				"type" 		=> "int",
				"field"  	=> "OrderItemID",
				"required"	=> true
			),
			"productID" => array(
				"name" 		=> "productID",
				"type" 		=> "int",
				"field"  	=> "OrderItemProductID",
				"required"	=> true
			),
			"orderID" => array(
				"name" 		=> "orderID",
				"type" 		=> "int",
				"field"  	=> "OrderItemOrderID",
				"required"	=> true
			),
			"amount" => array(
				"name" 		=> "amount",
				"type" 		=> "int",
				"field"  	=> "OrderItemAmount",
				"required"	=> true
			),
			"subtotalPrice" => array(
				"name" 		=> "subtotalPrice",
				"type" 		=> "float",
				"field"  	=> "OrderItemSubtotalPrice",
				"required"	=> true
			),
			"creationDate" => array(
				"name"		=> "creationDate",
				"type"		=> "datetime",
				"field"		=> "OrderItemCreationDate",
				"required"  => false
			),
			"updateDate" => array(
				"name"		=> "updateDate",
				"type"		=> "datetime",
				"field"		=> "OrderItemUpdateDate",
				"required"  => false
			),
			"status" => array(
				"name"		=> "status",
				"type"		=> "string",
				"field"		=> "OrderItemStatus",
				"values"	=> SELF::$statuses,
				"required"  => true,
				"default"	=> "active"
			)
		);
		
		$this->searchable = array(
			"name" => "contains",
			"id" => "equal"
		);
		$this->deleteAction = array(
			"field" => "status",
			"flag" => "deleted"
		);
		$this->sortingField = "displayOrder";
	}

	/**
	* Return a single Class object 
	* @return array Class Object
	*/
	public function getOne($param=null) {
		return $this->arrange(parent::getOne($param));
	}

	/**
	* Returns Class objects, matching the filter if any
	* @return array of objects
	*/
	
	public function get($param=null) {
		return $this->arrange(parent::get($param));
	}
	/**
	 * This function gets order id and returns rows which orderid=$data
	 * @param  int $data orderID
	 * @return array     rows which confirm this restriction
	 */
	public function findOrderItem($data){
		$this->setSorting("id asc");
		$this->addFilter("orderID",$data);
		$items=$this->arrange(parent::get());
		return $items;
	}
	
	

	/**
	 * This method gets specific information for order item object and adds
	 * @param int $orderID order id
	 * @param array $data  information like price or product id 
	 */
	public function addOne($orderID,$data){
		$item=array( 
			"productID" => $data["id"],
			"orderID"   => $orderID,
			"amount"    => $data["amount"],
			"subtotalPrice" =>$data["price"]
			);
		$result = $this->add($item);
	}
	/**
	* Adds a new object to the database
	* @param array $data
	* @return boolean
	*/

	public function add($data) {
		try {

			$this->create($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	/**
	 * This functions just calls delete method inside of Thing
	 * @param  int $data specific object id
	 * @return boolean    true if operation success
	 */
	public function destroy($data) {
		try {

			$this->delete($data);

			return true;//$this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}


	
	



	/**
	* Updates record on the database
	* @param array $data
	* @return boolean
	*/
	public function edit($data) {
		try {

			$this->load($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	
}
?>
