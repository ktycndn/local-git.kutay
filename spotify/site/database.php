<?php
define('DB_SERVERNAME', 'localhost'); 
define('DB_USERNAME','root');
define('DB_PASSWORD',''); 
define('DB_NAME','spotify');
/**
*
* @author Kutay Candan
* @version v1.1
*/
class Database{
	private $conn;





	/**
	 * This is construction function for Database class. It opens connection with database.
	 */
	function __construct(){
		try {
			$this->conn = new mysqli(DB_SERVERNAME,DB_USERNAME,DB_PASSWORD,DB_NAME);

		}
		catch(Exception $e){
			$this->createDatabase();
		}
		

	}
	/**
	 * This is destruction function for Database class. It closes connection with database.
	 */
	function __destruct(){
		try{
		mysqli_close($this->conn);
		}
		catch(Exception $e){
			
		}

	}
	


	/**
	 * This function creates spotify database if not exists.
	 */
	function createDatabase(){
		$conn = new mysqli(DB_SERVERNAME,DB_USERNAME,DB_PASSWORD);
		if($conn->connect_error){
			die("Connetion failed: " .$conn->connect_error);
		}

		$sql= "CREATE DATABASE IF NOT EXISTS spotify";
		if($conn->query($sql)){

		}else {
			echo "Error in creating database " . $conn->error;
		}
		$conn->close();
	}




	/**
	 * This function creates Token Table with its initalization if not exists.
	 */
	function createTokenTable(){
		$conn = $this->conn;
		
		$sql = "CREATE TABLE IF NOT EXISTS Token (id VARCHAR (30) NOT NULL, token VARCHAR(1000) NOT NULL)";
		if ($conn->query($sql)) {
		    //echo "Table Message created successfully";
			} else {
		    	echo "Error creating table: " . mysql_error();
			}
		$sql="SELECT * FROM Token Where id = '1'";
        $result=$conn->query($sql);
        if(!$row = mysqli_fetch_array($result)){
			$sql = "INSERT INTO Token "."(id, token) ". "VALUES ('1', '0')";
		    if($conn->query($sql)){

		    }else{
		      							
		    }
		}
	}
	/**
	 * This function take token from database.
	 * @return string token
	 */
	function takeToken(){
		$conn = $this->conn;
		$sql="SELECT * FROM Token Where id = '1'";
		$result=$conn->query($sql);
        if($row = mysqli_fetch_array($result)){
        	return $row["token"];


		}
	}
	/**
	 * This function changes token when it is demanded again.
	 * @param string token
	 */
	function setToken($token){
		$conn=$this->conn;
		echo $token;
		$sql = "UPDATE Token SET token='$token'  WHERE id = '1' ";
		if($conn->query($sql)){
				
			}else{
				
	      							
	      	}

	}















	/**
	 * This function creates Member Table if it is not exist.
	 */
	function createMemberTable(){
		$conn = $this->conn;
		$sql = "CREATE TABLE IF NOT EXISTS Member (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,user_id VARCHAR(255) NOT NULL, token VARCHAR(1000) NOT NULL)";
		if ($conn->query($sql)) {
		    //echo "Table Message created successfully";
			} else {
		    	echo "Error creating member table: " . mysql_error();
			}


	}
	/**
	 * This function add Member to Member table
	 * @param string user name from spotify api
	 */
	function addMember($user_id){
		$conn = $this->conn;
		$token=$this->takeToken();
		if($this->checkMember($user_id)){
			$sql = "INSERT INTO Member "."(user_id, token) ". "VALUES ('$user_id', '$token')";
			if($conn->query($sql)){

		    }else{
			      							
		    }
		}

	}
	/**
	 * This function check Member who is already in database or not.
	 * @param  string user name from spotify api
	 * @return boolean return true if it is not in database vice versa.
	 */
	function checkMember($user_id){

		$conn = $this->conn;
		$sql="SELECT * FROM Member WHERE user_id='$user_id'";
		$result=$conn->query($sql);
		
		
		if(mysqli_num_rows($result) == 0){
			return true;
		}else{
			return false;
		}

	}
	/**
	 * This function finds Members id number with using its user name
	 * @param  string user name from spotify api
	 * @return string member id
	 */
	function findMember($user_id){
		$conn = $this->conn;
		$sql="SELECT * FROM Member WHERE user_id='$user_id'";
		$result=$conn->query($sql);
		if (mysqli_num_rows($result) > 0) {
        	$row =mysqli_fetch_array($result);
        	return $row["id"];
		}
		return "-1";
	}








	/**
	 * This function creates Playlist table in spotify database.
	 */
	function createPlaylistTable(){
		$conn = $this->conn;
		$sql = "CREATE TABLE IF NOT EXISTS Playlist (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,member_id INT NOT NULL, playlist_id VARCHAR(255) NOT NULL,playlist_name VARCHAR(255) NOT NULL)";
		if ($conn->query($sql)) {
		    //echo "Table Message created successfully";
			} else {
		    	echo "Error creating plist table: " . mysql_error();
			}


	}
	/**
	 * This function adds Playlist to Playlist table.
	 * @param string user name from spotify api
	 * @param array playlist information(id,name)
	 */
	function addPlaylist($user_id,$playlist){
		$conn = $this->conn;
		$member_id=$this->findMember($user_id);
		$playlist_id=$playlist["playlist_id"];
		$playlist_name=$playlist["playlist_name"];
		if($this->checkPlaylist($member_id,$playlist_id)){
			$sql = "INSERT INTO Playlist "."(member_id, playlist_id, playlist_name) ". "VALUES ('$member_id', '$playlist_id', '$playlist_name')";
			if($conn->query($sql)){

		    }else{
			      							
		    }
		}

	}
	/**
	 * This function checks whether Playlist is already in database or not
	 * @param  string member id
	 * @param  string playlist id
	 * @return boolean return true if it is not in database vice versa
	 */
	function checkPlaylist($member_id,$playlist_id){

		$conn = $this->conn;
		$sql="SELECT * FROM Playlist WHERE member_id='$member_id' AND playlist_id='$playlist_id'";
		$result=$conn->query($sql);

		
		if(mysqli_num_rows($result) == 0){
			return true;
		}else{
			return false;
		}

		
      
	}
	/**
	 * This function finds playlist id in database with playlist id from spotify api
	 * @param  string playlist id from spotify api
	 * @return string playlist id from database
	 */
	function findPlaylist($playlist_id){
		$conn = $this->conn;
		$sql="SELECT * FROM Playlist WHERE playlist_id='$playlist_id'";
		$result=$conn->query($sql);
		if (mysqli_num_rows($result) > 0) {
        	$row =mysqli_fetch_array($result);
        	return $row["id"];
		}
		return "-1";
	}
	/**
	 * This function updates playlist id is playlist is rebuild in spotify
	 * @param  string user name from spotify api
	 * @param  string new playlist id from spotify api
	 * @param  string old playlist name from database
	 */
	function changePlaylist($user_id,$playlist_id,$playlist_name){
		$conn = $this->conn;
		$member_id=$this->findMember($user_id);
		$sql = "UPDATE Playlist SET playlist_id='$playlist_id'  WHERE member_id = '$member_id' and playlist_name = '$playlist_name'";
		if($conn->query($sql)){
				
			}else{
				echo "Cannot update playlist";
	      							
	      	}


	}










	/**
	 * This function creates Track table if not exists.
	 */
	function createTrackTable(){
		$conn = $this->conn;
		$sql = "CREATE TABLE IF NOT EXISTS Track (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,playlist_id VARCHAR(255) NOT NULL, song_id VARCHAR(255) NOT NULL,song_name VARCHAR(255) NOT NULL,artist_name VARCHAR(255) NOT NULL)";
		if ($conn->query($sql)) {
		    //echo "Table Message created successfully";
			} else {
		    	echo "Error creating track table: " . mysql_error();
			}


	}

	/**
	 * This function add song to Track table with using song information and playlist id
	 * @param array song informations(id,name,artist name)
	 * @param string playlist id from playlist database
	 */
	function addSong($song,$plist_id){
		//var_dump($song);
		//echo '<pre>';



		$conn = $this->conn;
		$playlist_id=$this->findPlaylist($plist_id);
		$song_id=$song["id"];
		$song_name=$song["name"];
		$artist_name=$song["artist"];
		/*var_dump($song_id);
		var_dump($song_name);
		var_dump($artist_name);*/
		if($this->checkSong($song_id,$playlist_id)){
			$sql = "INSERT INTO Track (playlist_id, song_id, song_name, artist_name) VALUES ('$playlist_id','$song_id', '$song_name', '$artist_name')";
			if($conn->query($sql)){
				//echo "Inserted".$song_name;

		    }else{
		    	echo "Cannot insert". $song_name; 
			      							
		    }
		}

	}
	/**
	 * This function checks whether song is already in Track table or not
	 * @param  string song id for checking
	 * @param  string playlist id from Playlist table for checking
	 * @return boolean return true if it is not in database vice versa
	 */
	function checkSong($song_id,$playlist_id){
		//echo $song_id."<br>";
		//echo $playlist_id;
		$conn = $this->conn;
		$sql="SELECT * FROM Track WHERE song_id='$song_id' AND playlist_id='$playlist_id'";
		$result=$conn->query($sql);
		
		if(mysqli_num_rows($result) == 0){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * This function gets all Songs from Track table with using playlist id
	 * @param  string user name to find playlist id
	 * @param  string playlist name to find playlist id
	 * @return array all songs for one playlist
	 */
	function getAllSongs($user_id,$playlist_name){
		$conn = $this->conn;
		$songarr=array();
		$playlist_id=$this->findPlaylistIDForSong($user_id,$playlist_name);
		$sql="SELECT * FROM Track WHERE playlist_id='$playlist_id'";
		$result=$conn->query($sql);
		if(mysqli_num_rows($result) >0){
			while ($row = mysqli_fetch_array($result)){
				$songarr[]=$row["song_id"];
			}
		}
		return $songarr;




	}
	/**
	 * This function finds playlist id with taking user name and playlist name
	 * @param  string user name for looking playlist id 
	 * @param  string playlist name for looking playlist id
	 * @return string playlist id
	 */
	function findPlaylistIDForSong($user_id,$playlist_name){
		$conn = $this->conn;
		$member_id=$this->findMember($user_id);
		$sql="SELECT * FROM Playlist WHERE member_id='$member_id' AND playlist_name='$playlist_name'";
		$result=$conn->query($sql);
		
		if(mysqli_num_rows($result) == 1){
			$row =mysqli_fetch_array($result);
        	return $row["id"];
		}
		echo "Cannot find playlist id for song";
		return "-1";


	}






	/**
	 * This function gets spotify playlist id with looking playlist name
	 * @param  string playlist name
	 * @return string spotify playlist id
	 */
	function getPlaylistID($playlist_name){
		$conn=$this->conn;
		$playlist_id="-1";
		$sql="SELECT * FROM Playlist WHERE playlist_name='$playlist_name'";
			$result=$conn->query($sql);
		if (mysqli_num_rows($result) == 1) {
        	$row =mysqli_fetch_array($result);
        	$playlist_id=$row["playlist_id"];
		}
		return $playlist_id;
	}
	/**
	 * This function check whether playlist can be deleted or not
	 * @param  string user name for deleting
	 * @param  string playlist name for finding playlist id
	 * @return boolean true if can be deleted or vice versa.
	 */
	function canDelete($user_id,$playlist_name){
		$conn=$this->conn;
		$playlist_id=$this->getPlaylistID($playlist_name);
		if($playlist_id=="-1"){
			echo "lol";
			return false;
		}
		if($this->checkMember($user_id)){
			echo "lol1";
			return false;
		}
		return true;


	}



















	


	






	
	

		

	
	
}














/*function showTable(){
        $conn= $this->conn;
        $sql="SELECT * FROM Message";
        $result=$conn->query($sql);
        while($row = mysqli_fetch_array($result)){
        	echo "<tr><td>".$row['topic']."</td>";
        	echo "<td>".$row['messageID']."</td>";
        	echo "<td>".$row['message']."</td>";
        	echo "<td>".$row['state']."</td>";
        	echo "<td>".$row['protocol']."</td>";
        	echo "<td>".$row['publishTime']."</td>";
        	echo "<td>".$row['subscribeTime']."</td></tr>";
        }
    }
    function sqlInsertWithPublish($topic,$messageID,$message){
		$conn=$this->conn;
		$date=$this->getDate();
		echo $date;
		
		$sql = "INSERT INTO Message "."(topic, messageID, message, protocol, publishTime) ". "VALUES ('$topic', '$messageID', '$message', 'MQTT', '$date')";
	    if($conn->query($sql)){
	      	echo "<h4 class=\"bg-success\" style=\"margin-top:20px;color:#333333\">You are successfully send message to system.</h4>";

	    }else{
	      							
	    }

	}
	function sqlUpdate($topic,$message){
		$conn=$this->conn;
		$date=$this->getDate();
		$msg=explode("!",$message);
		$sql = "UPDATE Message SET state=1, subscribeTime='$date'  WHERE topic = '$topic' and messageID = '$msg[0]' and message = '$msg[1]' ";
		if($conn->query($sql)){
				echo "<h4 class=\"bg-success\" style=\"margin-top:20px;color:#333333\">Succesfully gets message from broker.</h4>";
			}else{
	      							
	      	}
	}
	function checkIsSend(){
		$conn=$this->conn;
		$date=$this->getDate();
		$sql="SELECT * FROM Message WHERE state=0 and subscribeTime IS NULL";

		$result=$conn->query($sql);
		if (mysqli_num_rows($result) > 0){
  			while ($row = mysqli_fetch_array($result)){
  				$pdate=$row["publishTime"];

  				$diff = strtotime($date)-strtotime($pdate);
  				echo $diff;
  				if($diff>10){
  					$topic=$row["topic"];
  					$mid=$row["messageID"];
  					$msg=$row["message"];
  					$sql = "UPDATE Message SET state=1, subscribeTime='$date', protocol= 'SMS'  WHERE topic = '$topic' and messageID = '$mid' and message = '$msg' and publishTime ='$pdate'";
					if($conn->query($sql)){
						echo "<h4 class=\"bg-success\" style=\"margin-top:20px;color:#333333\">Succesfully gets message with SMS.</h4>";
					}else{
	      							
	      			}		


   				}
  			}
  		}
  	}*/

?>
	
