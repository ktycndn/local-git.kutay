<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WELCOME</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="index.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
            require_once("database.php");
        ?>
    	

        <div class="container">

            <div class="jumbotron" >
                <h1 > Welcome To Employment Provider </h1>
                
                <p> In this web site, we receive your information and business demands and give you the best job opportunities.</p>
                <a class="btn btn-lg btn-success" href="form.php" role="button" 
                >Apply now</a>
                <br>

                <p class="lead">Click, if you want to see list for all applied and employed people.</p>
                <a class="btn btn-lg btn-info" href="info.php" role="button">See all list</a>

            </div>


        </div>

    	

    </body>
</html> 