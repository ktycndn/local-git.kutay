<?php
define('DB_SERVERNAME', 'localhost'); 
define('DB_USERNAME','root');
define('DB_PASSWORD',''); 
define('DB_NAME','deneme');
/**
*
* @author Kutay Candan
* @version v1.0
*/
class Database{
	private $conn;
	/**
	 * This function is construction for database class. It connects database
	 */
	function __construct(){
		try {
			$this->conn = new mysqli(DB_SERVERNAME,DB_USERNAME,DB_PASSWORD,DB_NAME);

		}
		catch(Exception $e){
			return;
		}
		

	}
	/**
	 * This function is destruction for database class. It closes database.
	 */
	function __destruct(){
		mysqli_close($this->conn);
	}
	/**
	 * This function returns time and date for Istanbul.
	 * @return string date and time
	 */
	function getDate(){
			date_default_timezone_set("Europe/Istanbul"); 

			return date("Y-m-d H:i:s");
			
	}


	/**
	 * This function creates database if not exists.
	 */
	function createDatabase(){
		$conn = new mysqli(DB_SERVERNAME,DB_USERNAME,DB_PASSWORD);
		if($conn->connect_error){
			die("Connetion failed: " .$conn->connect_error);
		}

		$sql= "CREATE DATABASE IF NOT EXISTS deneme";
		if($conn->query($sql)){

		}else {
			echo "Error in creating database " . $conn->error;
		}
		$conn->close();
	}
	/**
	 * This function creates Message table which include all messages with specific protocol and status
	 */
	function createMessageTable(){
		$conn = new mysqli(DB_SERVERNAME,DB_USERNAME,DB_PASSWORD,DB_NAME);
		if($conn->connect_error){
			die("Connetion failed: " .$conn->connect_error);
		}
		$sql = "CREATE TABLE IF NOT EXISTS Message (topic VARCHAR(30) NOT NULL, messageID VARCHAR(30) NOT NULL,message VARCHAR(255), state BOOL NOT NULL DEFAULT '0', protocol VARCHAR(30),publishTime VARCHAR(30) NOT NULL, subscribeTime VARCHAR(30) )";
		if ($conn->query($sql)) {
		    //echo "Table Message created successfully";
			} else {
		    	echo "Error creating table: " . mysql_error();
			}
		$conn->close();

	}
	/**
	 * This function show Message table and list all columns in it.
	 */
	function showTable(){
        $conn= $this->conn;
        $sql="SELECT * FROM Message";
        $result=$conn->query($sql);
        while($row = mysqli_fetch_array($result)){
        	echo "<tr><td>".$row['topic']."</td>";
        	echo "<td>".$row['messageID']."</td>";
        	echo "<td>".$row['message']."</td>";
        	echo "<td>".$row['state']."</td>";
        	echo "<td>".$row['protocol']."</td>";
        	echo "<td>".$row['publishTime']."</td>";
        	echo "<td>".$row['subscribeTime']."</td></tr>";
        }
    }
    /**
     * This function adds database to new message when it is published
     * @param  string topic for message
     * @param  string message ID in message
     * @param  string the rest message
     */
    function sqlInsertWithPublish($topic,$messageID,$message){
		$conn=$this->conn;
		$date=$this->getDate();
		echo $date;
		
		$sql = "INSERT INTO Message "."(topic, messageID, message, protocol, publishTime) ". "VALUES ('$topic', '$messageID', '$message', 'MQTT', '$date')";
	    if($conn->query($sql)){
	      	echo "<h4 class=\"bg-success\" style=\"margin-top:20px;color:#333333\">You are successfully send message to system.</h4>";

	    }else{
	      							
	    }

	}
	/**
	 * This function updates messages when whether subscriber gets message
	 * @param  string topic for message
	 * @param  string all message
	 */
	function sqlUpdate($topic,$message){
		$conn=$this->conn;
		$date=$this->getDate();
		$msg=explode("!",$message);
		$sql = "UPDATE Message SET state=1, subscribeTime='$date'  WHERE topic = '$topic' and messageID = '$msg[0]' and message = '$msg[1]' ";
		if($conn->query($sql)){
				echo "<h4 class=\"bg-success\" style=\"margin-top:20px;color:#333333\">Succesfully gets message from broker.</h4>";
			}else{
	      							
	      	}
	}
	/**
	 * This function checks whether message is taken by MQTT if it not. Sends by SMS.
	 * So update database table like that.
	 */
	function checkIsSend(){
		$conn=$this->conn;
		$date=$this->getDate();
		$sql="SELECT * FROM Message WHERE state=0 and subscribeTime IS NULL";

		$result=$conn->query($sql);
		if (mysqli_num_rows($result) > 0){
  			while ($row = mysqli_fetch_array($result)){
  				$pdate=$row["publishTime"];

  				$diff = strtotime($date)-strtotime($pdate);
  				echo $diff;
  				if($diff>10){
  					$topic=$row["topic"];
  					$mid=$row["messageID"];
  					$msg=$row["message"];
  					$sql = "UPDATE Message SET state=1, subscribeTime='$date', protocol= 'SMS'  WHERE topic = '$topic' and messageID = '$mid' and message = '$msg' and publishTime ='$pdate'";
					if($conn->query($sql)){
						echo "<h4 class=\"bg-success\" style=\"margin-top:20px;color:#333333\">Succesfully gets message with SMS.</h4>";
					}else{
	      							
	      			}		


   				}
  			}
  		}
  	}
}











?>