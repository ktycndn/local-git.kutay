<?php
require_once(__DIR__.'/../config/config.php');
header('Content-Type: application/json');

try {

	$products = new Product();

	if(!isset($_GET["name"]))
		throw new Exception("No ID provided");


	$product = $products->getInfo($_GET["name"]);

	$response = array();
	$response["data"] = $product;
	$response["result"] = true;

	echo json_encode($response);

} catch(Exception $e) {
	stop($e->getMessage());
}

?>