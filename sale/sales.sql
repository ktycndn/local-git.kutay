SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;



DROP TABLE IF EXISTS `Product`;
CREATE TABLE `Product` (
  `ProductID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(255) DEFAULT NULL,
  `ProductPrice` numeric(8,2) DEFAULT NULL,
  `ProductCreationDate` datetime DEFAULT NULL,
  `ProductUpdateDate` datetime DEFAULT NULL,
  `ProductStatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ProductID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Member`;
CREATE TABLE `Member` (
  `MemberID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MemberFirstName` varchar(255) DEFAULT NULL,
  `MemberLastName` varchar(255) DEFAULT NULL,
  `MemberMail` varchar(255) DEFAULT NULL,
  `MemberPassword` varchar(255) DEFAULT NULL,
  `MemberCreationDate` datetime DEFAULT NULL,
  `MemberUpdateDate` datetime DEFAULT NULL,
  `MemberStatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `OrderItem`;
CREATE TABLE `OrderItem` (
  `OrderItemID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OrderItemProductID` int(10) DEFAULT NULL,
  `OrderItemOrderID` int(10) DEFAULT NULL,
  `OrderItemAmount` int(10) DEFAULT NULL,
  `OrderItemSubtotalPrice` numeric(8,2) DEFAULT NULL,
  `OrderItemCreationDate` datetime DEFAULT NULL,
  `OrderItemUpdateDate` datetime DEFAULT NULL,
  `OrderItemStatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`OrderItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Orders`;
CREATE TABLE `Orders` (
  `OrderID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OrderMemberID` int(10) DEFAULT NULL,
  `OrderTotalPrice` numeric(8,2) DEFAULT NULL,
  `OrderCreationDate` datetime DEFAULT NULL,
  `OrderUpdateDate` datetime DEFAULT NULL,
  `OrderStatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;













