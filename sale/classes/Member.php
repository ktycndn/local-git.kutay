<?php
/**
 * Class: Member
 * @author Kutay Candan
 * This is Member object sale product.
 */ 
class Member extends Thing {

	static $statuses = array(
		0	=> "draft",
		1	=> "active",
		2	=> "passive",
		3 	=> "deleted"
	);

	public function __construct() {
		$this->name = "member";
		$this->table = "Member";
		$this->prefix = "Member";
		$this->fields = array(
			"id" => array(
				"name" 		=> "id",
				"type" 		=> "int",
				"field"  	=> "MemberID",
				"required"	=> true
			),
			"firstName" => array(
				"name" 		=> "firstName",
				"type" 		=> "string",
				"field"  	=> "MemberFirstName",
				"required"	=> false
			),
			"lastName" => array(
				"name" 		=> "lastName",
				"type" 		=> "string",
				"field"  	=> "MemberLastName",
				"required"	=> false
			),
			"mail" => array(
				"name" 		=> "mail",
				"type" 		=> "string",
				"field"  	=> "MemberMail",
				"required"	=> true
			),
			"password" => array(
				"name" 		=> "password",
				"type" 		=> "string",
				"field"  	=> "MemberPassword",
				"required"	=> true
			),
			"creationDate" => array(
				"name"		=> "creationDate",
				"type"		=> "datetime",
				"field"		=> "MemberCreationDate",
				"required"  => false
			),
			"updateDate" => array(
				"name"		=> "updateDate",
				"type"		=> "datetime",
				"field"		=> "MemberUpdateDate",
				"required"  => false
			),
			"status" => array(
				"name"		=> "status",
				"type"		=> "string",
				"field"		=> "MemberStatus",
				"values"	=> SELF::$statuses,
				"required"  => true,
				"default"	=> "active"
			)
		);
		
		$this->searchable = array(
			"name" => "contains",
			"id" => "equal"
		);
		$this->deleteAction = array(
			"field" => "status",
			"flag" => "deleted"
		);
		$this->sortingField = "displayOrder";
	}

	/**
	* Return a single Class object 
	* @return array Class Object
	*/
	public function getOne($param=null) {
		return $this->arrange(parent::getOne($param));
	}

	/**
	* Returns Class objects, matching the filter if any
	* @return array of objects
	*/
	
	public function get($param=null) {
		return $this->arrange(parent::get($param));
	}
	
	
	
	
	


	/**
	* Adds a new object to the database
	* @param array $data
	* @return boolean
	*/

	public function add($data) {
		try {

			$this->create($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	/**
	 * This functions just calls delete method inside of Thing
	 * @param  int $data specific object id
	 * @return boolean    true if operation success
	 */
	public function destroy($data) {
		try {

			$this->delete($data);

			return true;//$this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	
	/**
	* Updates record on the database
	* @param array $data
	* @return boolean
	*/
	public function edit($data) {
		try {

			$this->load($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	/**
	 * This function gets users mail and pw then check he is signed in or not
	 */
	public function login($mail="",$password=""){

		
		
		$this->addFilter("mail",$mail);
		$this->addFilter("password",$password);
		$data=$this->arrange(parent::get());

		if(sizeof($data)==1){
			echo "People is registered";
			setcookie("mail", $mail, time() + (60*60*24));
  			setcookie("password", $password, time() + (60*60*24));
			setcookie("memberID",$data[0]["id"] , time() + (60*60*24));
			if($data[0]["firstName"]!=null){
				setcookie("firstName",$data[0]["firstName"] , time() + (60*60*24));
			}
			else{
							}
			header("Location: memberpage.php");
		}
		else{
			echo "Not registered";

		}
		
	}
	/**
	 * This method checks user is log in or not
	 * @return [type] [description]
	 */
	public function checkLogin(){
		$memberID=$_COOKIE["memberID"];
		$password=$_COOKIE["password"];
		$this->addFilter("id",$memberID);
		$this->addFilter("password",$password);
		$data=$this->arrange(parent::get());
		if(sizeof($data)==1){
			return true;
		}
		else{
			return false;
		}
	}


	
}




























?>
