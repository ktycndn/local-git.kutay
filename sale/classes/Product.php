<?php
/**
 * Class: Product
 * @author Kutay Candan
 * This is Product object sale product.
 */ 
class Product extends Thing {

	static $statuses = array(
		0	=> "draft",
		1	=> "active",
		2	=> "passive",
		3 	=> "deleted"
	);

	public function __construct() {
		$this->name = "product";
		$this->table = "Product";
		$this->prefix = "Product";
		$this->fields = array(
			"id" => array(
				"name" 		=> "id",
				"type" 		=> "int",
				"field"  	=> "ProductID",
				"required"	=> true
			),
			"name" => array(
				"name" 		=> "name",
				"type" 		=> "string",
				"field"  	=> "ProductName",
				"required"	=> true
			),
			"price" => array(
				"name" 		=> "price",
				"type" 		=> "float",
				"field"  	=> "ProductPrice",
				"required"	=> true
			),
			"creationDate" => array(
				"name"		=> "creationDate",
				"type"		=> "datetime",
				"field"		=> "ProductCreationDate",
				"required"  => false
			),
			"updateDate" => array(
				"name"		=> "updateDate",
				"type"		=> "datetime",
				"field"		=> "ProductUpdateDate",
				"required"  => false
			),
			"status" => array(
				"name"		=> "status",
				"type"		=> "string",
				"field"		=> "ProductStatus",
				"values"	=> SELF::$statuses,
				"required"  => true,
				"default"	=> "active"
			)
		);
		
		$this->searchable = array(
			"name" => "contains",
			"id" => "equal"
		);
		$this->deleteAction = array(
			"field" => "status",
			"flag" => "deleted"
		);
		$this->sortingField = "displayOrder";
	}

	/**
	* Return a single Class object 
	* @return array Class Object
	*/
	public function getOne($param=null) {
		return $this->arrange(parent::getOne($param));
	}

	/**
	* Returns Class objects, matching the filter if any
	* @return array of objects
	*/
	
	public function get($param=null) {
		return $this->arrange(parent::get($param));
	}
	/**
	 * This method finds row specific to product name
	 * @param  string $name product name
	 * @return array       entire row matched with name.
	 */
	public function getInfo($name){
		$this->addFilter("name",$name);
		$data=$this->arrange(parent::get());
		return $data;
	}

	/**
	 * This method lists all product object
	 */
	public function getAll(){
		$this->setSorting("id asc");
		$data=$this->get();
		return $data;

	}
	

	/**
	* Adds a new object to the database
	* @param array $data
	* @return boolean
	*/

	public function add($data) {
		try {

			$this->create($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	/**
	 * This functions just calls delete method inside of Thing
	 * @param  int $data specific object id
	 * @return boolean    true if operation success
	 */
	public function destroy($data) {
		try {

			$this->delete($data);

			return true;//$this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	
	/**
	* Updates record on the database
	* @param array $data
	* @return boolean
	*/
	public function edit($data) {
		try {

			$this->load($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	
}
?>
