<?php
/**
 * Class: RelatedObject
 * @package YOYO
 * @author Emre Buyukozkan <emre.buyukozkan@driveyoyo.com> 
 * Test RelatedObject Class for playground demos.
 * @method object getOne()
 * @method array of object get()
 */ 
class RelatedObject extends Thing {

	static $statuses = array(
		0	=> "draft",
		1	=> "active",
		2	=> "passive",
		3 	=> "deleted"
	);

	public function __construct() {
		$this->name = "relatedObject";
		$this->table = "RelatedObjects";
		$this->prefix = "RelatedObject";
		$this->fields = array(
			"id" => array(
				"name" 		=> "id",
				"type" 		=> "int",
				"field"  	=> "RelatedObjectID",
				"required"	=> true
			),
			"name" => array(
				"name" 		=> "name",
				"type" 		=> "string",
				"field"  	=> "RelatedObjectName",
				"required"	=> true
			),
			"value" => array(
				"name" 		=> "value",
				"type" 		=> "string",
				"field"  	=> "RelatedObjectValue",
				"required"	=> true
			),
			"creationDate" => array(
				"name"		=> "creationDate",
				"type"		=> "datetime",
				"field"		=> "RelatedObjectCreationDate",
				"required"  => false
			),
			"updateDate" => array(
				"name"		=> "updateDate",
				"type"		=> "datetime",
				"field"		=> "RelatedObjectUpdateDate",
				"required"  => false
			),
			"status" => array(
				"name"		=> "status",
				"type"		=> "string",
				"field"		=> "RelatedObjectStatus",
				"values"	=> SELF::$statuses,
				"required"  => true,
				"default"	=> "active"
			)
		);
		$this->searchable = array(
			"name" => "contains",
			"id" => "equal"
		);
		$this->deleteAction = array(
			"field" => "status",
			"flag" => "deleted"
		);
		$this->sortingField = "displayOrder";
	}

	/**
	* Return a single Class object 
	* @return array Class Object
	*/
	public function getOne($param=null) {
		return $this->arrange(parent::getOne($param));
	}

	/**
	* Returns Class objects, matching the filter if any
	* @return array of objects
	*/
	public function get($param=null) {
		return $this->arrange(parent::get($param));
	}

	/**
	* Adds a new object to the database
	* @param array $data
	* @return boolean
	*/

	public function add($data) {
		try {

			$this->create($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}

	/**
	* Updates record on the database
	* @param array $data
	* @return boolean
	*/
	public function edit($data) {
		try {

			$this->load($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	
}
?>