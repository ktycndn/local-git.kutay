<?php
/**
 * Class: Object
 * @package YOYO
 * @author Emre Buyukozkan <emre.buyukozkan@driveyoyo.com> 
 * Test Object Class for playground demos.
 * @method object getOne()
 * @method array of object get()
 */ 
class Object extends Thing {

	static $statuses = array(
		0	=> "draft",
		1	=> "active",
		2	=> "passive",
		3 	=> "deleted"
	);

	public function __construct() {
		$this->name = "object";
		$this->table = "Objects";
		$this->prefix = "Object";
		$this->fields = array(
			"id" => array(
				"name" 		=> "id",
				"type" 		=> "int",
				"field"  	=> "ObjectID",
				"required"	=> true
			),
			"name" => array(
				"name" 		=> "name",
				"type" 		=> "string",
				"field"  	=> "ObjectName",
				"required"	=> true
			),
			"value" => array(
				"name" 		=> "value",
				"type" 		=> "string",
				"field"  	=> "ObjectValue",
				"required"	=> true
			),
			"numericValue" => array(
				"name" 		=> "numericValue",
				"type" 		=> "int",
				"field"  	=> "ObjectNumericValue",
				"required"	=> false
			),
			"creationDate" => array(
				"name"		=> "creationDate",
				"type"		=> "datetime",
				"field"		=> "ObjectCreationDate",
				"required"  => false
			),
			"updateDate" => array(
				"name"		=> "updateDate",
				"type"		=> "datetime",
				"field"		=> "ObjectUpdateDate",
				"required"  => false
			),
			"status" => array(
				"name"		=> "status",
				"type"		=> "string",
				"field"		=> "ObjectStatus",
				"values"	=> SELF::$statuses,
				"required"  => true,
				"default"	=> "active"
			),
			"relatedObjectID" => array(
				"name"		=> "relatedObjectID",
				"type"		=> "int",
				"field"		=> "ObjectRelatedObjectID",
				"required"  => false,
			),
			"displayOrder" => array(
				"name"		=> "displayOrder",
				"type"		=> "int",
				"field"		=> "ObjectDisplayOrder",
				"required"  => false,
			)
		);
		$this->relations = array(
			"relatedObjectID" => array(
				"class"		=> "RelatedObject",
				"table"		=> "RelatedObjects",
				"field"		=> "RelatedObjectID"
			)
		);
		$this->searchable = array(
			"name" => "contains",
			"id" => "equal"
		);
		$this->deleteAction = array(
			"field" => "status",
			"flag" => "deleted"
		);
		$this->sortingField = "displayOrder";
	}

	/**
	* Return a single Class object 
	* @return array Class Object
	*/
	public function getOne($param=null) {
		return $this->arrange(parent::getOne($param));
	}

	/**
	* Returns Class objects, matching the filter if any
	* @return array of objects
	*/
	
	public function get($param=null) {
		return $this->arrange(parent::get($param));
	}
	/**
	 * This function gest all row which's numeric value between 40 and 50
	 * @return array of these objects
	 */
	public function get40_50($param=null) {
		$this->addFilter("numericValue",40,">=");
		$this->addFilter("numericValue",50,"<=");
		$this->setSorting("numericValue asc");
		return $this->arrange(parent::get($param));
	}
	/**
	 * This function finds row which's numeric value are less then 20 and changes their status 3
	 */
	public function deleteless20ID($param=null){
		
		$this->addFilter("numericValue",20,"<=");
		//$this->addFilter("status",1,"=");
		$ids=array();

		$data=$this->arrange(parent::get($param));
		//var_dump($data);
		for($i=0;$i<sizeof($data);$i++){
			$ids[]=(int)$data[$i]["id"];
			//echo $data[$i]["id"]."<br>";

		}
		if(sizeof($ids)>0)
			$this->destroyALL($ids);
	}
	/**
	 * This function changes name to name-updated if its numeric value is odd
	 */
	public function updateOdd($param=null){
		$data=$this->arrange(parent::get($param));
		
		for($i=0;$i<sizeof($data);$i++){
			if($data[$i]["numericValue"]%2==1){
				$name=$data[$i]["name"]."-updated";
				$arr=array("id"=>$data[$i]["id"],"name"=>$name);
				$this->edit($arr);
			}

		}



		
	}



	/**
	* Adds a new object to the database
	* @param array $data
	* @return boolean
	*/

	public function add($data) {
		try {

			$this->create($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	/**
	 * This functions just calls delete method inside of Thing
	 * @param  int $data specific object id
	 * @return boolean    true if operation success
	 */
	public function destroy($data) {
		try {

			$this->delete($data);

			return true;//$this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	/**
	 * This function inserts 100 row with specific name
	 * @param [type] $data [description]
	 */
	public function add_hundred($data) {
		$name=$data["name"];
		for($i=1;$i<101;$i++){
			$data["name"] = $name.$i;
			$data["numericValue"]=$i;
			$this->add($data);

		}
	}
	/**
	 * This function delete all row which has id in $ids
	 * @param  array $ids Object id array
	 */
	public function destroyAll($ids){
		for($i=0;$i<=sizeof($ids);$i++){
			
			$this->destroy($ids[$i]);
		}





	}
	



	/**
	* Updates record on the database
	* @param array $data
	* @return boolean
	*/
	public function edit($data) {
		try {

			$this->load($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	
}
?>
