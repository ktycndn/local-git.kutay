<!DOCTYPE html>
<html lang="en">
    <head>
        <title>FORM PAGE</title>
        <meta charset="utf-8">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="form.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
    	
        <?php
            include "Employee.php";
            $idle_employee= new Employee(); $jobs = $idle_employee->getJobs();
            $cmpe=$jobs[0];
            $me=$jobs[1];
            $ee=$jobs[2];
            $ie=$jobs[3];
            echo "  <div class=\"container\">
        <div class=\"jumbotron\">
          <h2>Register Here </h2>
          <br>
          <form action=".htmlspecialchars($_SERVER["PHP_SELF"])." method=\"post\">
          <div class=\"form-group\">
            <label for=\"fname\">First Name:</label>
            <input type=\"text\" class=\"form-control\" id=\"fname\" placeholder=\"Enter first name\" name=\"first\" required>
          </div>
          <div class=\"form-group\">
            <label for=\"lname\">Last Name:</label>
            <input type=\"text\" class=\"form-control\" id=\"lname\" placeholder=\"Enter last name\" name=\"last\" required>
          </div>
          <div class=\"form-group\">
            <label for=\"idn\">Identity Number:</label>
            <input type=\"text\" class=\"form-control\" id=\"idn\" placeholder=\"Enter identity number\" name=\"idnum\" required>
          </div>
          <div class=\"form-group\">
            <label for=\"byear\">Birth Year:</label>
            <input type=\"text\" class=\"form-control\" id=\"byear\" placeholder=\"Enter birth year\" name=\"birth\" required>
          </div>
          <div class=\"form-group\">
            <label for=\"occ\">Jobs:</label>
            
            <select name=\"job\" class=\"form-control\" id=\"occ\" >
                    <option value=\"Computer Engineering\">".$cmpe."</option>
                    <option value=\"Mechanical Engineering\">".$me."</option>
                    <option value=\"Electrical Engineering\">".$ee."</option>
                    <option value=\"Industrial Engineering\">".$ie."</option>
                  </select> 
                        ";
            if(isset($_POST["first"]) && isset($_POST["last"]) && isset($_POST["idnum"]) && isset($_POST["birth"]) && isset($_POST["job"])){
                $firstname = $_POST["first"];
                $lastname = $_POST["last"];
                $identitynumber = $_POST["idnum"];
                $birthyear = $_POST["birth"];
                $joboptions = $_POST["job"];

                if(empty($_POST["first"]) || empty($_POST["last"]) || empty($_POST["idnum"]) || empty($_POST["birth"]) || empty($_POST["job"])){
                    echo    "Some part is missing. Please fill all parts";
                }else{
                    $person = new People();
                    $person->addPeople($firstname,$lastname,$identitynumber,$birthyear,$joboptions);

                }




            }
            else{

                
            }
            echo "    <input type=\"submit\" class=\"btn btn-success btn-lg\" value=\"Register\"/>

                  <a class=\"btn btn-lg btn-warning\" href=\"index.php\" role=\"button\">Go back</a>
          </div>
          
                


        </div>
       </div>";

        ?>

         

              



        

    	

    </body>
</html>