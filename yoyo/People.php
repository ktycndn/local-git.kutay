<?php 
	/**
	*
	* @author Kutay Candan
	* @version v1.1
	*/
    class People {

    	
		private $conn;


		protected $id, $firstName, $lastName,$identityNumber, $birthYear,$applicationCount, $lastApplicationDate;


		/**
		 * This function construct a class. It starts database connection.
		 *
		 */
		function __construct(){
			$db_sname='localhost';
			$db_uname='root';
			$db_pw='';
			$db_name='Yoyo';
			$this->conn=new mysqli($db_sname,$db_uname,$db_pw,$db_name);
		}


		/**
		 * This function destruct a class. It closes database connection.
		 *
		 */
		function __destruct(){
			mysqli_close($this->conn);
		}

		/**
		 * This function allows to get variables.
		 * @return <type> any type
		 */
		function __get($name){
			return $this->$name;
		}


		/**
		 * This function changes string to uppercase. Also it works with Turkish chars.
		 * @param string $name mixed upper and lower case letter.
		 * @return string Upper case string
		 */
		function upperCase($name){ 
    		return mb_convert_case(str_replace('i','İ',$name), MB_CASE_UPPER, "UTF-8"); 
		} 


		/**
		 * This function returns today's date.
		 * @return string returns Date
		 */
		function getDate(){
			date_default_timezone_get('UTC');
			return date('j F Y');
		}


		/**
		 * This function checks People is whether in table or not.
		 * @param int $identitynumber requires persons id number
		 * @return boolean true if it is in table.
		 */
		function isPeopleOnTable($identitynumber){
			$conn=$this->conn;
			$sql="SELECT * FROM People WHERE identityNumber= '$identitynumber'";
			return mysqli_num_rows($conn->query($sql)) > 0;
		}

		/**
		 * This function inserts People to sql table
		 * @param string $firstname People's first name
		 * @param string $lastname People's last name
		 * @param int $identitynumber People's identity number
		 * @param int $birthyear People's birth year
		 * @param string $selectedjob People's preferred job for working
		 */
		function sqlInsertPeopleTable($firstname,$lastname,$identitynumber,$birthyear,$selectedjob){
			$conn=$this->conn;
			$applicationCount=1;
			$lastApplicationDate =$this->getDate();

			$sql = "INSERT INTO People ".
	      								"(firstName, lastName, identityNumber, birthYear, applicationCount, lastApplicationDate, preferredJob) ".
	      									"VALUES ('$firstname', '$lastname', '$identitynumber', '$birthyear', '$applicationCount', '$lastApplicationDate', '$selectedjob')";
	      	if($conn->query($sql)){
	      		echo "<h4 class=\"bg-success\" style=\"margin-top:20px;color:#333333\">You are successfully added to system.</h4>";

	      	}else{
	      							
	      	}

		}


		/**
		 * This function updates People table for already applied people
		 * @param int $identitynumber People's identity number
		 * @param string $selectedjob People's new preffered job for working
		 */
		function sqlUpdatePeople($identitynumber,$selectedjob){
			$conn=$this->conn;
			$lastApplicationDate =$this->getDate();
			$sql = "UPDATE People SET preferredJob='$selectedjob',applicationCount=applicationCount+1, lastApplicationDate='$lastApplicationDate' WHERE identityNumber = '$identitynumber'";
			if($conn->query($sql)){
				echo "<h4 class=\"bg-warning\" style=\"margin-top:20px;color:#333333\">You are already in system. Your application is reorganized.</h4>";
			}else{
	      							
	      	}

		}
		/**
		 * This function returns People Table
		 * @return array returns all information on People Table
		 */
		function sqlSelectPeopleTable(){
			$conn=$this->conn;
			$resArr=array();


			$sql="SELECT * FROM People";
      		$result=$conn->query($sql);
        	if ($result->num_rows > 0) {
          		while($row = $result->fetch_assoc()) {
          			$resArr[]=$row;
				}
         	}
         	return $resArr;


		}


		/**
		 * This function checks whether People is working or not.
		 * @param int $personId People's identity number
		 * @return boolean true if People is working
		 */
		function isPeopleWorking($personId){
			$conn=$this->conn;

			$sql="SELECT * FROM Employee WHERE personID='$personId' AND employmentStatus='Working'";
			return mysqli_num_rows($conn->query($sql)) > 0;


		}
		
		/**
		 * This function checks People's information is correct on server and client side
		 * @return boolean true if informations are correct.
		 */		
		function checkPeople(){

			$id=$this->identityNumber;
			$first=$this->firstName;
			$last=$this->lastName;
			$birth=$this->birthYear;
						
			$control=false;
			
			if(is_numeric($id) && is_numeric($birth)){
						
				$id*=1;
				$birth*=1;

				if(is_int($id) && is_int($birth)){
					if($id < 100000000000 && $id > 9999999999 && $birth > 1850 && $birth < 2018){
					$control=true;
								
			}}}
			if(!$control){
						
				echo "<h4 class=\"bg-danger\" style=\"margin-top:20px;color:#333333\">Your identity information is wrong. Please enter correct informations.</h4>";
				return false;
			}
			
			try{
						
				$client=new SoapClient("https://tckimlik.nvi.gov.tr/Service/KPSPublic.asmx?WSDL");
				$result = $client->TCKimlikNoDogrula(array( 'TCKimlikNo' => $id,'Ad' => $first,'Soyad' => $last,'DogumYili' => $birth));
    			if($result->TCKimlikNoDogrulaResult){
        			return true;
    			}else{
        			echo "<h4 class=\"bg-danger\" style=\"margin-top:20px;color:#333333\">Your identity information is wrong. Please enter correct informations.</h4>";
        			return false;
    			}
    		}catch(SoapFault $exception){
				#echo $exception->getMessage();
				echo "<h4 class=\"bg-danger\" style=\"margin-top:20px;color:#333333\">Your identity information is wrong. Please enter correct informations.</h4>";
				return false;

			}

		}


		/**
		 * This function adds or updates People
		 * @param string $firstname People's first name
		 * @param string $lastname People's last name
		 * @param int $identitynumber People's identity number
		 * @param int $birthyear People's birth year
		 * @param string $selectedjob People's preferred job for working
		 */		
		function addPeople($firstname,$lastname,$identitynumber,$birthyear,$selectedjob){
			$conn = $this->conn;

			$firstname=mysqli_real_escape_string($conn,$firstname);
			$lastname=mysqli_real_escape_string($conn,$lastname);
			$identitynumber=mysqli_real_escape_string($conn,$identitynumber);
			$birthyear=mysqli_real_escape_string($conn,$birthyear);

			$firstname=$this->upperCase($firstname);
			$lastname=$this->upperCase($lastname);

			$this->firstName=$firstname;
			$this->lastName=$lastname;
			$this->identityNumber=$identitynumber;
			$this->birthYear=$birthyear;
						
						
			if($this->checkPeople()){
							
				if ($this->isPeopleOnTable($identitynumber)) {
					$this->sqlUpdatePeople($identitynumber,$selectedjob);			
				}
				else{
					$this->sqlInsertPeopleTable($firstname,$lastname,$identitynumber,$birthyear,$selectedjob);
				}
			}
			else {
							
			}
						

		}
		/**
		* This function displays People Table in table.
		*
		*/
		function showPeopleTable(){

			
			$peopleTable=$this->sqlSelectPeopleTable();
			
			if($peopleTable){
				
				$i=0;
				for($i;$i<sizeof($peopleTable);$i++){
					echo "<tr>
                  		<td>" . $peopleTable[$i]["id"] . "</td>
                  		<td>" . $peopleTable[$i]["firstName"] . "</td>
                  		<td>" . $peopleTable[$i]["lastName"] . "</td>
                  		<td>" . $peopleTable[$i]["identityNumber"] . "</td>
                  		<td>" . $peopleTable[$i]["birthYear"] . "</td>
                  		<td>" . $peopleTable[$i]["applicationCount"] . "</td>
                  		<td>" . $peopleTable[$i]["lastApplicationDate"] . "</td>";
                  	$personId=$peopleTable[$i]["id"];
                  	if($this->isPeopleWorking($personId)){
                  		echo "<td></td></tr>";

                  	}
                  	else{
                  		echo "<td> <form action=". htmlspecialchars($_SERVER["PHP_SELF"]) ." method=\"post\">" .
                    	    "<input type=\"submit\" class=\"btn btn-success\"   value=\"HIRE\" onclick=\"HIRE\"> ".
                    	    "<input type=\"hidden\" name=\"id\" value=". $personId.">".
                    	    "</form> ". "</td></tr>";

                  	}

				}


			}
			
			
		}

		
	}
			

    ?>

	  	  		