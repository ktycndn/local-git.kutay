<?php

require 'vendor/autoload.php';
include 'database.php';
/**
*
* @author Kutay Candan
* @version v1.1
*/

class Playlist{
	private $api;
	private $token;
	private $db;
	/**
	 * This is construction for Playlist class.
	 * It creates connection with SpotifyWebAPI
	 * Also creates new Database object.
	 */
	function __construct(){
		$this->api=new SpotifyWebAPI\SpotifyWebAPI();
		$db = new Database();
		$this->db=$db;
		$this->token=$db->taketoken();

	}
	/**
	 * This function just gets spotify information and prints.
	 * @param  string user name(user id)
	 */
	function showPlaylist($id){
		$api=$this->api;
		$api->setAccessToken($this->token);
		$playlists = $api->getUserPlaylists($id, [ 'limit' => 5]);
		foreach ($playlists->items as $playlist) {
			echo $playlist->name;
    		echo '<a href="' . $playlist->external_urls->spotify . '">' . $playlist->id . '</a> <br>';
    		$playlistTracks = $api->getUserPlaylistTracks($id, $playlist->id);

			foreach ($playlistTracks->items as $track) {
			    $track = $track->track;

			    echo '<a href="' . $track->external_urls->spotify . '">' . $track->name ."   " .$track->id. '</a> <br>';
			}
		}

	}
	
	/**
	 * This function creates checks if playlist is in 
	 * our database. If it is, creates playlist with
	 * same name. Then include all songs for this playlist.
	 * @param  string user name for connect spotify
	 * @param  string playlist name to check whether is in our database
	 */
	function getBackPlaylist($user_id,$playlist_name){
		$db=$this->db;
		$api=$this->api;
		$api->setAccessToken($this->token);
		//echo $user_id."<br>".$playlist_name;
		
		if($db->getPlaylistID($playlist_name)=="-1"){
			echo "This playlist name is not in our database";
			return;
		}
		else{
			$api->createUserPlaylist($user_id, ['name' => $playlist_name]);
		}
		$playlist_id=$this->changePlaylistID($user_id,$playlist_name);
		
		if($playlist_id=="-1"){
			echo "Error to get playlist_id";
		}
		else{
			$songs=$db->getAllSongs($user_id,$playlist_name);
			$api->addUserPlaylistTracks($user_id, $playlist_id, $songs);
		}
		


	}
	/**
	 * This function changes old playlist id to new playlist id in database.
	 * Also returns new playlist id
	 * @param  string user name to connect spotify
	 * @param  string playlist name to get playlist id
	 * @return string new playlist id
	 */
	function changePlaylistID($user_id,$playlist_name){
		$db=$this->db;
		$api=$this->api;
		$api->setAccessToken($this->token);
		
		$playlists = $api->getUserPlaylists($user_id/*, [ 'limit' => 5]*/);
		$playlist_id="-1";
		foreach ($playlists->items as $playlist) {
			
			if($playlist_name==$playlist->name){
				$playlist_id=$playlist->id;
				$db->changePlaylist($user_id,$playlist_id,$playlist_name);

			}
			
			
		}
		return $playlist_id;







	}

	/**
	 * This function delete specific playlist in spotify.
	 * @param  string user name to connect spotify
	 * @param  string playlist name to get playlist id
	 */
	function deletePlaylist($user_id,$playlist_name){
		$api=$this->api;
		$api->setAccessToken($this->token);
		$db=$this->db;

		
		if($db->canDelete($user_id,$playlist_name)){
			$playlist_id=$db->getPlaylistID($playlist_name);
			//echo "Silindi<br>";
			//echo $user_id;
			//echo $playlist_id;
			$api->unfollowPlaylist($user_id, $playlist_id);
		}


	}

	/**
	 * This function get user name and save all his playlist and its songs.
	 * @param string user name for connect spotify
	 */
	function addAllInformations($user_id){
		$db=$this->db;
		$api=$this->api;
		$api->setAccessToken($this->token);
		//Member Is Added Here
		$db->addMember($user_id);
		$playlists = $api->getUserPlaylists($user_id/*, [ 'limit' => 5]*/);
		
		foreach ($playlists->items as $playlist) {
			$playlist_arr=array(
				'playlist_id'=>$playlist->id ,
				'playlist_name'=>$playlist->name
				);
			$songs=$this->getSongs($user_id,$playlist->id);
			//Playlist Is Added Here
			$db->addPlaylist($user_id,$playlist_arr);
			//Songs are added here
			//echo sizeof($songs);
			for($i=0;$i<sizeof($songs);$i++){
				$db->addSong($songs[$i],$playlist->id);
			}
			
		}

	}
	
	/**
	 * This function gets user name and playlist id and returns all songs with their informations.
	 * @param  string user name to connect spotify
	 * @param  string playlist id to get all songs from spotify api
	 * @return array all songs in one playlist with their informations
	 */
	function getSongs($user_id,$playlist_id){
		$api=$this->api;
		$api->setAccessToken($this->token);
		$playlistTracks = $api->getUserPlaylistTracks($user_id, $playlist_id);
		$songarr=array();
		foreach ($playlistTracks->items as $track) {
				
    			$track = $track->track;
    			$artist= $track->artists;

    			$artists_name=$artist[0]->name;
    			$artists_name=str_replace("'", "\'", $artists_name);
    			$track_id=str_replace("'", "\'", $track->id);
    			$track_name=str_replace("'", "\'", $track->name);
    			

    			$song=array(
    				'id'=>$track_id,
    				'name'=>$track_name,
    				'artist'=>$artists_name
    				);
    			//echo '<a href="' . $track->external_urls->spotify . '">' . $track->name . '</a> <br>';
    			$songarr[]=$song;
    		}
    		return $songarr;
	}



	



}











?>