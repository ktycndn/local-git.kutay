

<?php
/*
    'f30c7dd6c91946a3b0bdf18b2b0c4419',
    '3005ef334e514db696d8c936a3aecbe2',
    'localhost:80/callback'*/

require '../vendor/autoload.php';

$session = new SpotifyWebAPI\Session(
    'f30c7dd6c91946a3b0bdf18b2b0c4419',
    '3005ef334e514db696d8c936a3aecbe2',
    'http://local.spotify.api.com/callback/taketoken.php'
);

$options = [
    'scope' => [
        'playlist-read-private',
        'user-read-private',
        'playlist-modify-private',
        'playlist-modify-public'
    ],
];

header('Location: ' . $session->getAuthorizeUrl($options));
//die();

?>