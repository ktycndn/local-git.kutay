<?php
	include "People.php";
	/**
	*
	* @author Kutay Candan
	* @version v1.1
	*/
	class Employee extends People{

		protected $id, $personID, $employmentStartDate, $positionName,$employmentStatus;
		protected $jobs = array('Computer Enginnering', 'Mechanical Enginnering','Electrical Enginnering', 'Industrial Engineering');
					
		/**
		 * This function gets job list.
		 * @return array Job array
		 */
		function getJobs(){
			return $this->jobs;
		}


		/**
		 * This function finds People's preferred job
		 * @param int $personID People's identity number
		 * @return string People's preferred job
		 */
		function findPrefJob($personID){
			$conn=$this->conn;
			$sql="SELECT preferredJob FROM People WHERE id='$personID'" ;
	    	$result=$conn->query($sql);
      		$row = mysqli_fetch_assoc($result);
      		return $row["preferredJob"];
		}


		/**
		 * This function checks People works at least one time before.
		 * @param int $personID People's identity number
		 * @return boolean true if he works before
		 */
		function isAlreadyWorked($personID){
			$conn=$this->conn;
			$sql="SELECT * FROM Employee WHERE personID='" . $personID . "'";
			if (mysqli_num_rows($conn->query($sql)) > 0){
				return true;
			}
			return false;
		}


		/**
		 * This function checks People is fired or not.
		 * @param int $personID People's identity number
		 * @return boolean true if he is fired
		 */
		function isFired($personID){
			$conn=$this->conn;
			$sql="SELECT * FROM Employee WHERE personID='$personID' AND employmentStatus='Fired'";
			if (mysqli_num_rows($conn->query($sql)) > 0){
				return true;
			}
			return false;
		}
		/**
		 * This function updates Employee as working. So it gives a job.
		 * @param int $personID People's identity number
		 */
		function reworkedEmployee($personID){
			$conn=$this->conn;
			$preferredJob=$this->findPrefJob($personID);
			
			$sql = "UPDATE Employee SET employmentStatus='Working',positionName='$preferredJob'  WHERE personID = '$personID'";
						
	   		if($conn->query($sql)){
	    		echo "";
	      	}else{
	      		echo "Cannot add";
	      	}
		}
		/**
		 * This function fires Employee.
		 * @param int $id Employee's id
		 */
		function removeEmployee($id){
					
			$conn=$this->conn;
			$sql = "UPDATE Employee SET employmentStatus='Fired'  WHERE id = '$id'";
			if($conn->query($sql)){
	      		echo "";
	      	}else{
	      		echo "Cannot add1";
	      	}
							
		}

		/**
		 * This function inserts Employee to Employee table.
		 * @param int $personID People's identity number
		 */
		function sqlInsertEmployeeTable($personID){
			$conn=$this->conn;
			$preferredJob=$this->findPrefJob($personID);
			$employmentStartDate=$this->getDate();
			$sql = "INSERT INTO Employee ".
	      				"(personID, employmentStartDate, positionName, employmentStatus) ".
	      				"VALUES ('$personID', '$employmentStartDate', '$preferredJob', 'Working')";
	      	if($conn->query($sql)){
	      		echo "";
	      	}else{
	      		echo "Cannot add";
	   		}
		}
		

		/**
		 * This function returns Employee Table
		 * @return array returns all information on Employee Table
		 */	
		function sqlSelectEmployeeTable(){
			$conn=$this->conn;
			$resArr=array();


			$sql="SELECT * FROM Employee";
      		$result=$conn->query($sql);
        	if ($result->num_rows > 0) {
          		while($row = $result->fetch_assoc()) {
          			$resArr[]=$row;
				}
         	}
         	return $resArr;
		}
				

		/**
		 * This function adds or updates Employee
		 * @param int $personID People's identity number
		 */		
		function addEmployee($personID){
					
			$isAlreadyWorked=$this->isAlreadyWorked($personID);
			$isFired=$this->isFired($personID);

			if($isFired){
				$this->reworkedEmployee($personID);
			}
			if(!$isAlreadyWorked){
				$this->sqlInsertEmployeeTable($personID);
						
			}

		}
		/**
		* This function displays Employee Table in table.
		*
		*/		
		function showEmployeeTable(){
			$conn=$this->conn;
			$employeeTable=$this->sqlSelectEmployeeTable();
			
			if($employeeTable){
				
				$i=0;
				for($i;$i<sizeof($employeeTable);$i++){
					echo "<tr>
                  			<td>" . $employeeTable[$i]["id"] . "</td>
               	  			<td>" . $employeeTable[$i]["personID"] . "</td>
                  			<td>" . $employeeTable[$i]["employmentStartDate"] . "</td>
                  			<td>" . $employeeTable[$i]["positionName"] . "</td>
                  			<td>" . $employeeTable[$i]["employmentStatus"] . "</td>";
                  	$personID=$employeeTable[$i]["personID"];
                  	if(!$this->isFired($personID)){
                  		echo "<td><form action=". htmlspecialchars($_SERVER["PHP_SELF"]) ." method=\"post\">" ."<input type=\"submit\" class=\"btn btn-danger\"   value=\"FIRE\" onclick=\"FIRE\"> "."<input type=\"hidden\" name=\"e-id\" value=". $employeeTable[$i]["id"].">"."</form> ". "</td></tr>"; 

                  	}
                  	else{
                  		echo "<td></td></tr>"; 

                  	}
				}
			}
		}
				
	}






?>