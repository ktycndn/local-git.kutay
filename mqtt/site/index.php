<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WELCOME</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="index.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
        include "database.php";
        $db =new Database();
        $db->createDatabase();
        $db =new Database();
        $db->createMessageTable();
            
        ?>
    	

        <div class="container">

            <div class="jumbotron" >
                <h1 > Publisher and Subscriber Demo </h1>
                
                <p> Send new messages from here.</p>
                <a class="btn btn-lg btn-success" target="_blank" href="publish.php" role="button" 
                >Publish</a>
                <br>
                <p> Open subscriber from here.</p>
                <a class="btn btn-lg btn-success" target="_blank" href="subscribe.php" role="button" 
                >Subscribe</a>
                <br>

                <p class="lead">All Messages are here.</p>
                <a class="btn btn-lg btn-info" href="table.php" role="button">See All Messages</a>

            </div>


        </div>

    	

    </body>
</html> 