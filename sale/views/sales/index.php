<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WELCOME</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="index.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
        require_once(__DIR__.'/../../config/config.php');
            
        ?>
    	

        <div class="container">

            <div class="jumbotron" >
                <h1 > Sale Test Project </h1>
                
                <p> Log in from here.</p>
                <a class="btn btn-lg btn-success"  href="login.php" role="button" 
                >Log In</a>
                <br>
                
                <p> List all products.</p>
                <a class="btn btn-lg btn-info" href="listproducts.php" role="button">List All</a>
                <br>

                <!--<p class="lead">All Messages are here.</p>
                <a class="btn btn-lg btn-info" href="table.php" role="button">See All Messages</a>
                -->

            </div>


        </div>

    	

    </body>
</html> 