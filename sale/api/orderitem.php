<?php
require_once(__DIR__.'/../config/config.php');
header('Content-Type: application/json');

try {

	$orderItem = new OrderItem();

	if(!isset($_GET["orderid"]))
		throw new Exception("No ID provided");


	$oi = $orderItem->findOrderItem($_GET["orderid"]);

	$response = array();
	$response["data"] = $oi;
	$response["result"] = true;

	echo json_encode($response);

} catch(Exception $e) {
	stop($e->getMessage());
}

?>