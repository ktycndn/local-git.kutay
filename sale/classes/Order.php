<?php
/**
 * Class: Order
 * @author Kutay Candan 
 * This is Order object sale product.
 */ 
class Order extends Thing {

	static $statuses = array(
		0	=> "draft",
		1	=> "active",
		2	=> "passive",
		3 	=> "deleted"
	);

	public function __construct() {
		$this->name = "order";
		$this->table = "Orders";
		$this->prefix = "Order";
		$this->fields = array(
			"id" => array(
				"name" 		=> "id",
				"type" 		=> "int",
				"field"  	=> "OrderID",
				"required"	=> true
			),
			"memberID" => array(
				"name" 		=> "memberID",
				"type" 		=> "int",
				"field"  	=> "OrderMemberID",
				"required"	=> true
			),
			"totalPrice" => array(
				"name" 		=> "totalPrice",
				"type" 		=> "float",
				"field"  	=> "OrderTotalPrice",
				"required"	=> true
			),
			"creationDate" => array(
				"name"		=> "creationDate",
				"type"		=> "datetime",
				"field"		=> "OrderCreationDate",
				"required"  => false
			),
			"updateDate" => array(
				"name"		=> "updateDate",
				"type"		=> "datetime",
				"field"		=> "OrderUpdateDate",
				"required"  => false
			),
			"status" => array(
				"name"		=> "status",
				"type"		=> "string",
				"field"		=> "OrderStatus",
				"values"	=> SELF::$statuses,
				"required"  => true,
				"default"	=> "active"
			)
		);
		/*$this->relations = array(
			"memberID" => array(
				"class"		=> "Member",
				"table"		=> "Member",
				"field"		=> "MemberID"
			)
		);*/
		$this->searchable = array(
			"name" => "contains",
			"id" => "equal"
		);
		$this->deleteAction = array(
			"field" => "status",
			"flag" => "deleted"
		);
		$this->sortingField = "displayOrder";
	}

	/**
	* Return a single Class object 
	* @return array Class Object
	*/
	public function getOne($param=null) {
		return $this->arrange(parent::getOne($param));
	}

	/**
	* Returns Class objects, matching the filter if any
	* @return array of objects
	*/
	
	public function get($param=null) {
		return $this->arrange(parent::get($param));
	}
	
	



	/**
	* Adds a new object to the database
	* @param array $data
	* @return boolean
	*/

	public function add($data) {
		try {

			$this->create($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	/**
	 * This functions just calls delete method inside of Thing
	 * @param  int $data specific object id
	 * @return boolean    true if operation success
	 */
	public function destroy($data) {
		try {

			$this->delete($data);

			return true;//$this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	/**
	 * This method activates when user wants to buy item in his cart.
	 * @param  array $data all datas in cookies
	 */
	public function finishOrder($data){
		if(sizeof($data)==0){
			echo "You don't add any item please add some item then buy";
			return false;
		}

		$memberID = (int)$_COOKIE["memberID"];
		

		$totalprice=0;
		for($i=0;$i<sizeof($data);$i++){
			$totalprice += $data[$i]["price"];

		}
		//echo $memberID. "  ". $totalprice;
		$orderData=array(
			"memberID" => $memberID,
			"totalPrice" => $totalprice
			);
		//var_dump($orderData);
	    $result=$this->add($orderData);
		if($result) {
			$orderID = $this->getInsertID();
			for($i=0;$i<sizeof($data);$i++){
				$orderitem= new OrderItem();
				$orderitem->addOne($orderID,$data[$i]);
			}
			//$item = array();
			//$item["orderID"] = $orderID;
			//$orderItem->add($item);
		}
		header("Location: refreshcookies.php");


	}
	
	



	/**
	* Updates record on the database
	* @param array $data
	* @return boolean
	*/
	public function edit($data) {
		try {

			$this->load($data);
			return $this->persist();
		} catch(Exception $e) {
			$this->errorMessage = $e->getMessage();
			return false;
		}
	}
	/**
	 * This method lists all order object
	 */
	public function getAll(){
		$this->setSorting("id asc");
		$data=$this->get();
		return $data;

	}
	
}
?>
